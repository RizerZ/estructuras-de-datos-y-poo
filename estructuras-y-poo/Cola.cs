﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace estructuras_y_poo
{
    class Cola
    {
        private Nodo primero;

        public void Encolar(int dato)
        {
            if (primero == null)
            {
                primero = new Nodo(dato);
            }
            else
            {
                Nodo aux = BuscarUltimo(primero);
                aux.siguiente = new Nodo(dato);
            }
        }

        private Nodo BuscarUltimo(Nodo nodo)
        {
            if (nodo.siguiente == null)
            {
                return nodo;
            }
            else
            {
                return BuscarUltimo(nodo.siguiente);
            }
        }

        public void Desencolar()
        {
            if(primero==null)
            {
                Console.WriteLine("Ya no hay elementos en la cola");
            }
            else
            {
                if (primero.siguiente != null)
                {
                    primero = primero.siguiente;
                }
                else
                {
                    primero = null;
                    Console.WriteLine("Ultimo elemento de la cola sacado");
                }
            }
            
        }

        public bool Vacia()
        {
            return (primero == null);
        }

        public Nodo Primero()
        {
            if(primero!=null)
            {
                return primero;
            }
            return null;
        }
    }
}
