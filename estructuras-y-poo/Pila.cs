﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace estructuras_y_poo
{
    class Pila
    {
        Nodo tope;

        public Nodo Tope()
        {
            return tope;
        }

        public void Apilar(int dato)
        {
            if (tope==null)
            {
                tope = new Nodo(dato);
            }
            else
            {
                Nodo aux = tope;
                tope = new Nodo(dato);
                tope.siguiente = aux;
            }
        }

        public void Desapilar()
        {
            if (tope!=null)
            {
                tope = tope.siguiente;
            }
            else
            {
                Console.WriteLine("La pila ya está vacía");
            }

        }
    }
}
