﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace estructuras_y_poo
{
    public class Nodo
    {
        public int dato { get; set; }
        public Nodo siguiente { get; set; }

        public Nodo(int _dato)
        {
            this.dato = _dato;
            this.siguiente = null;
        }
    }
}
