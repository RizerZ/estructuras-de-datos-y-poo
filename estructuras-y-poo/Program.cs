﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace estructuras_y_poo
{
    class Program
    {

        static void Main(string[] args)
        {
            #region ListaEnlazada
            Console.WriteLine("Funcionamiento de la lista enlazada: ");
            Lista lista = new Lista();
            lista.ListaVacia();
            lista.AddNodo(1);
            lista.AddNodo(11);
            lista.AddNodo(12);
            lista.AddNodo(15);
            lista.AddNodo(17);
            lista.AddNodo(18);
            lista.AddNodo(19);
            lista.AddNodo(1);
            lista.Listar();
            Console.WriteLine("Tamaño " + lista.Tamaño());
            lista.ListaVacia();
            Console.WriteLine();
            #endregion

            #region Pila
            Console.WriteLine("Funcionamiento de la pila: ");
            Pila pila = new Pila();
            pila.Apilar(1);
            pila.Apilar(8);
            pila.Apilar(15);

            pila.Desapilar();
            pila.Desapilar();
            pila.Desapilar();
            pila.Desapilar();
            Console.WriteLine();
            #endregion

            #region Cola
            Console.WriteLine("Funcionamiento de la cola: ");
            Cola cola = new Cola();
            cola.Encolar(10);
            cola.Encolar(8);
            cola.Encolar(6);

            if(!cola.Vacia())
            {
                Console.WriteLine("La cola contiene elementos");
            }

            cola.Desencolar();
            cola.Desencolar();
            cola.Desencolar();
            cola.Desencolar();

            Console.WriteLine();

            if(cola.Vacia())
            {
                Console.WriteLine("La cola Está vacía");
            }

            Console.ReadLine();

            #endregion

        }


    }
}
