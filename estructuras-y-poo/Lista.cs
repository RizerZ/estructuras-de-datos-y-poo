﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace estructuras_y_poo
{
    public class Lista
    {
        private Nodo primero;
        private int tamaño;
        public Lista()
        {
            this.primero = null;
            this.tamaño = 0;
        }

        public void AddNodo(int dato)
        {
            Nodo nuevo = new Nodo(dato);
            nuevo.siguiente = primero;
            primero = nuevo;
            tamaño++;
        }

        public int Tamaño()
        {
            return tamaño;
        }

        public void Listar()
        {
            Nodo actual = primero;
            while (actual != null)
            {
                Console.Write("[" + actual.dato + "]->");
                actual = actual.siguiente;
            }
            Console.WriteLine();
        }

        public void ListaVacia()
        {
            if (primero == null)
            {
                Console.WriteLine("La lista está vacía");
            }
            else
            {
                Console.WriteLine("La lista tiene datos");
            }
        }
    }
}

